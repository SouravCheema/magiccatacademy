package com.example.magiccatacademy;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.Display;
import android.view.WindowManager;

public class Background {

    private Bitmap image;
    private int x,y;
    Context context;

    public Background(Bitmap img) {
        image = img;
    }

    public void update() {


    }

    public void draw(Canvas canvas) {


        canvas.drawBitmap(image,x,y,null);
    }
}
