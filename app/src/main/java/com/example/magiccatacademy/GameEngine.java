package com.example.magiccatacademy;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

public class GameEngine extends SurfaceView implements SurfaceHolder.Callback, GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener  {



    public static final int WIDTH = 856;
    public static final int HEIGHT = 480;

    private Background bg;
    private Player player;

    private MainThread thread;
    public GameEngine(Context context) {
    super(context);
    thread = new MainThread(getHolder(),this);
    getHolder().addCallback(this);
    setFocusable(true);

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        bg = new Background(BitmapFactory.decodeResource(getResources(),R.drawable.ghosts));
        player = new Player(BitmapFactory.decodeResource(getResources(),R.drawable.catattack1),90,45,3);
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

        boolean retry = true;
        /**
         *The join() method is used to hold the execution of currently running
         *thread until the specified thread is dead(finished execution).
         */
        while(retry)
        {
            try{
                thread.setRunning(false);
/**
 *What is a join thread..?
 */
                thread.join();

            }catch (InterruptedException e){e.printStackTrace();}




        }//end while

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    public void update() {
        if(player.getPlaying()) {
            //bg.update();
            player.update();
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        final float scaleFactorX = (getWidth() / WIDTH) * 1.f;
        final float scaleFactorY = (getWidth() / HEIGHT) * 1.f;

        if (canvas != null) {
            final int savedState = canvas.save();
            canvas.scale(scaleFactorX, scaleFactorY);
            bg.draw(canvas);
            player.draw(canvas);
            canvas.restoreToCount(savedState);
        }


    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
//        mTextView.setText("On Single Tap Confirmed");
        if(!player.getPlaying())
        {
            player.setPlaying(true);
            Log.d("sensors","tapped");
        }
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
//        mTextView.setText("On Double Tap");
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent event) {
//        mTextView.setText("On Double Tap Event");
        return true;
    }

    @Override
    public boolean onDown(MotionEvent event) {
//        mTextView.setText("On Down");
        return true;
    }

    @Override
    public void onShowPress(MotionEvent event) {
//        mTextView.setText("On Show Press");
    }

    @Override
    public boolean onSingleTapUp(MotionEvent event) {
//        mTextView.setText("On Single Tap Up");
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent event, MotionEvent event1, float v, float v1) {
//        mTextView.setText("On Scroll");
        return true;
    }

    @Override
    public void onLongPress(MotionEvent event) {
//        mTextView.setText("On Long Press");
    }

    @Override
    public boolean onFling(MotionEvent event, MotionEvent event1, float v, float v1) {
//        mTextView.setText("On Fling");
        return true;
    }
}
