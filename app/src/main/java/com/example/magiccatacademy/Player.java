package com.example.magiccatacademy;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class Player extends gameObject{

    private Bitmap spritesheet;


    private int score;


    private double dya;

    private boolean playing;

    private Animation animation  = new Animation();
    private long startTime;

    public Player(Bitmap res, int w, int h, int numFrames) {
        x = 100;
        y = GameEngine.HEIGHT/2;
        score = 0;

        height = h;
        width = w;

        Bitmap[] image = new Bitmap[numFrames];
        spritesheet = res;

        for (int i = 0; i < image.length; i++)
        {

            image[i] = Bitmap.createBitmap(spritesheet, i*width, 0, width, height);
        }
        animation.setFrames(image);
        animation.setDelay(10);

        //Now we initiate the timer so we can use in the update method
        startTime = System.nanoTime();

    }

    public void update() {
        long elapsed = (System.nanoTime()-startTime)/1000000;


        if(elapsed>100)
        {
            score++;
            startTime = System.nanoTime();
        }
        animation.update();
    }

        public void draw(Canvas canvas) {
            canvas.drawBitmap(animation.getImage(),x,y,null);
        }

    public int getScore(){
        return score;
    }
    public boolean getPlaying(){
        return playing;
    }
    public void setPlaying(boolean b){
        playing = b;
    }
    public void resetScore(){
        score = 0;
    }

}
