package com.example.magiccatacademy;

import android.graphics.Rect;

public abstract class gameObject {

    protected int x;
    protected int y;
    protected int dx;
    protected int dy;
    protected int width;
    protected int height;


    public void setX (int x) {
        this.x = x;
    }

    public int getX(int x) {
        return y;
    }

    public void setY (int y) {
        this.y = y;
    }

    public int getY(int y) {
        return y;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public Rect getRecytnagle() {
        return new Rect(x,y,x + width,y+height);
    }

}
